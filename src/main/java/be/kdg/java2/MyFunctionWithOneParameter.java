package be.kdg.java2;

@FunctionalInterface
public interface MyFunctionWithOneParameter<T> {
    void theFunction(T parameter);
}
