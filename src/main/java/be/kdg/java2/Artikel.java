package be.kdg.java2;

import java.util.Random;

public class Artikel {
    private final int id;
    private final String merk;
    private final String type;
    private final double prijs;

    public Artikel(int id, String merk, String type, double prijs) {
        this.id = id;
        this.merk = merk;
        this.type = type;
        this.prijs = prijs;
    }

    public static Artikel random(){
        Random random = new Random();
        return new Artikel(random.nextInt(),
                "merk" + random.nextInt(), "type" + random.nextInt(),
                random.nextDouble() * 1500);
    }

    public int getId() {
        return id;
    }

    public String getMerk() {
        return merk;
    }

    public String getType() {
        return type;
    }

    public double getPrijs() {
        return prijs;
    }

    @Override
    public String toString() {
        return "Artikel{" +
                "id=" + id +
                ", merk='" + merk + '\'' +
                ", type='" + type + '\'' +
                ", prijs=" + prijs +
                '}';
    }
}
