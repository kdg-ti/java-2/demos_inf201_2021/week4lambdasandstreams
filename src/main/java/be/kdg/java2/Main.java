package be.kdg.java2;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void voerDeFunctieUit(MyFunction myFunction) {
        System.out.println("Ik ga de functie uitvoeren:");
        myFunction.theFunction();
        System.out.println("Uitgevoerd!");
    }

    public static <T> void voerDeFunctieMetEenParameterUit(MyFunctionWithOneParameter myFunction, T parameter) {
        System.out.println("Ik ga de functie uitvoeren:");
        myFunction.theFunction(parameter);
        System.out.println("Uitgevoerd!");
    }

    public static <T> List<T> filter(List<T> entries, Predicate<T> predicate) {
        List<T> filteredList = new ArrayList<>();
        for (T entry : entries) {
            if (predicate.test(entry)) filteredList.add(entry);
        }
        return filteredList;
    }

    public static void main(String[] args) {
       /* System.out.println("Hello world");
        voerDeFunctieUit(() -> System.out.println("Hallo allemaal!"));
        voerDeFunctieUit(() -> System.out.println("Ik ben een andere functie"));
        List<MyFunction> myFunctions = new ArrayList<>();
        myFunctions.add(() -> System.out.println("Ik ben functie1"));
        myFunctions.add(() -> System.out.println("Ik ben functie2"));
        myFunctions.add(() -> System.out.println("Ik ben functie3"));
        for (MyFunction myFunction : myFunctions) {
            voerDeFunctieUit(myFunction);
        }
        MyFunctionWithOneParameter function
                = parameter -> System.out.println("Hello " + parameter);
        for (int i=0;i<10;i++) {
            voerDeFunctieMetEenParameterUit(function, "Jos" + i);
        }*/
      /*  Predicate<Artikel> isExpensive = artikel -> artikel.getPrijs()>500;
        List<Artikel> artikels = filter(Artikels.getArtikels(), isExpensive);
        for (Artikel artikel : artikels) {
            System.out.println(artikel);
        }
        List<String> names = Arrays.asList("Jos", "Dirk", "Maria", "Josefien", "Lucas");
        List<String> filtered = filter(names, name -> name.length()>4);
        for (String s : filtered) {
            System.out.println(s);
        }
        artikels = Artikels.getArtikels();
        Collections.sort(artikels, Comparator.comparing(Artikel::getMerk));
        artikels.forEach(artikel -> {
            System.out.println("Hello!");
            System.out.println(artikel);
        });*/
        //creatie van een stream - tussentijdse operaties - afsluitende operaties
        Artikels.getArtikels().stream()
                .filter(artikel -> artikel.getPrijs() < 500)
                .filter(artikel -> artikel.getMerk().equals("Lenovo"))
                .forEach(System.out::println);
        //hoeveel artikels zijn duurder dan 500 euro?
        int count = 0;
        for (Artikel artikel : Artikels.getArtikels()) {
            if (artikel.getPrijs() > 500) {
                count++;
            }
        }
        System.out.println(count);
        System.out.println(Artikels.getArtikels().stream()
                .filter(artikel -> artikel.getPrijs() < 500)
                .count());
        Stream.of("Jos", "Jef", "Marie").forEach(System.out::println);
        Artikel artikel = Stream.generate(() -> {
                    Random random = new Random();
                    return new Artikel(random.nextInt(),
                            "merk" + random.nextInt(), "type" + random.nextInt(),
                            random.nextDouble() * 1500);
                })
                .limit(100)
                .parallel()
                .max(Comparator.comparing(Artikel::getPrijs)).get();
        System.out.println("Duurste " + artikel);
        //.collect(Collectors.joining(", "));
        //System.out.println(list);
        int minimum = Stream.generate(() -> new Random().nextInt(100))
                .limit(100)
                .min(Comparator.comparingInt(i -> i)).orElse(-1);//Dit geeft een Optional?

        System.out.println("Kleinste: " + minimum);
        System.out.println("Klassiek:");
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
        }
        System.out.println("Met streams:");
        Stream.iterate(0, i -> i + 1).limit(100).parallel().forEach(i -> {
            System.out.println(i);
        });
        
    }
}
